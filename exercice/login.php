<?php
require_once 'include/functions.php';
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if (isset($_SESSION['auth'])){
    header('location: index.php');
    exit();
}

if (!empty($_POST) && !empty($_POST['username']) && !empty($_POST['password'])){
    require_once 'include/db.php';

    $req = $pdo->prepare('SELECT * FROM users WHERE (username = :username OR email = :username)');
    $req->execute(['username' => $_POST['username']]);
    $user=$req->fetch();
    if(password_verify($_POST['password'], $user->password)){
        $_SESSION['auth'] = $user;
        $_SESSION['flash']['success'] = 'Bienvenue '. $_SESSION['auth']->username;
        header('location: index.php');
        exit();
    }else{
        $_SESSION ['flash']['danger'] = 'Identifiant ou mot de passe incorrect';
    }
}
?>
<?php require 'include/header.php'; ?>

<div class="formulaire">
    <h1>Connexion</h1>
    <form action="" method="POST">
        <label for="username"></label>
        <input type="text" id="username" name="username" placeholder="Your name or email..">

        <label for="password"></label>
        <input type="password" id="password" name="password" placeholder="Your password..">

        <input type="submit" value="Connexion">
    </form>
</div>

<?php require 'include/footer.php'; ?>

