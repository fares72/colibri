<?php
//informations de connexion a la base données
$pdo = new PDO('mysql:dbname=colibri;host=localhost', '','');
//permet d'envoyer les erreurs
$pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);