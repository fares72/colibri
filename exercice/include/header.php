<?php
if (session_status() == PHP_SESSION_NONE){
    session_start();
}
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title>COLIBRI</title>
</head>
<body>

    <header> <!-- start of the header of the page -->
        <div id='logo'>
            <a href='front_page.php'>MON SUPER SITE</a><!-- link to the Accueil page -->
        </div>

        <nav> <!-- start of the header nav menu -->
            <ul>
                <?php if (isset($_SESSION['auth'])): ?> <!--on verifie que l'utilisateur est bien authentifier-->
                <li class='active'>
                    <a href='index.php'>Accueil</a><!-- link to the Connexion page-->
                </li>
                <li>
                    <a href='logout.php'>Se deconnecter</a><!-- link to the Inscription page-->
                </li>

                <?php else: ?><!--sinon on ne lui affiche que se connecter et s'inscrire-->
                <li class='active'>
                    <a href='login.php'>Connexion</a><!-- link to the Connexion page-->
                </li>
                <li>
                    <a href='register.php'>Inscription</a><!-- link to the Inscription page-->
                </li>
                <?php endif; ?>
            </ul>
        </nav> <!-- end of the header nav menu -->
    </header> <!-- end of the page header -->

    <?php if (isset($_SESSION['flash'])): ?>
        <?php foreach ($_SESSION['flash'] as $type => $message): ?>
            <div class="alert alert-<?= $type; ?>">
                <?= $message; ?>
            </div>
        <?php endforeach; ?>
        <?php unset($_SESSION['flash']); ?>
    <?php endif; ?>




