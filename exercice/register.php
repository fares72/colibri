
<?php
require_once 'include/functions.php';
session_start();
if (!empty($_POST)){

    $errors =array();
    require_once 'include/db.php';


    if (empty($_POST['username']) || !preg_match('/^[a-zA-Z0-9_]+$/', $_POST['username'])){
        $errors['username'] = "Votre pseudo n'est pas valide";
    }else{
        $req = $pdo->prepare('SELECT id FROM users WHERE username = ?');
        $req->execute([$_POST['username']]);
        $user = $req->fetch();

        if ($user){
            $errors['username'] = 'Ce pseudo est deja pris';
        }
    }

    if (empty($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
        $errors['email'] = "Votre email n'est pas valide";
    }else{
        $req = $pdo->prepare('SELECT id FROM users WHERE email = ?');
        $req->execute([$_POST['email']]);
        $user = $req->fetch();

        if ($user){
            $errors['email'] = 'Cet email est deja utilisé pour un autre compte';
        }
    }

    if (empty($_POST['password']) || $_POST['password'] != $_POST['password_confirm']){
        $errors['password'] = "Vous devez entrer un mot de passe valide";
    }

    if (empty($errors)){
        $req = $pdo->prepare("INSERT INTO users SET username = ?, 
                      password = ?, email = ?");

        $password = password_hash($_POST['password'], PASSWORD_BCRYPT);

        $req->execute([$_POST['username'], $password, $_POST['email']]);

        $user_id = $pdo->lastInsertId();

        header('location: login.php');
        exit();

    }
}

?>

<?php

require 'include/header.php'; ?>



    <?php if (!empty($errors)) : ?>
        <div class="alert alert-danger">
            <p>Vous n'avez pas rempli le formulaire correctement </p>
            <ul>
                <?php foreach ($errors as $error): ?>
                    <li><?= $error ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
    <div class="formulaire">
        <h1>Inscription</h1>
        <form action="" method="POST">
            <label for="username"></label>
            <input type="text" id="username" name="username" placeholder="Your username..">

            <label for="email"></label>
            <input type="text" id="email" name="email" placeholder="Your email..">

            <label for="password"></label>
            <input type="password" id="password" name="password" placeholder="Your password..">

            <label for="password_confirm"></label>
            <input type="password" id="password_confirm" name="password_confirm" placeholder="Confirm password..">

            <input type="submit" value="Inscription">
        </form>
    </div>
<?php require 'include/footer.php'; ?>

