# Petit exercice pour vérifier vos compétences en php/sql/html/css

## Objectif : Mise en place d'une architecture MVC  

## Pages à créer :

* Accueil avec une mise en page responsive :
    - exemple de rendu : exemple.png et exemple-responsive.png (vous n'êtes pas obligé de suivre cet exemple)
    - fichier index.html si vous souhaitez récupérer le texte
* Inscription et Connexion


### Rendu du projet sous Git (lien du clone) transmis par mail à robin@agence-colibri.com